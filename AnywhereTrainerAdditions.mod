<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >

	<UiMod name="AnywhereTrainerAdditions" version="1.0" date="21-Jul-17 3:54:38">
  	<Author name="anon" email="" />
    <Description text="Adds bank (ctrl+LClick to quickly put item to your inventory), mail (only sending or receiving already seen letters - useful to dump all junk you want to sell to alts while you're farming) and auction (you will still have to find a real mailbox to receive anything) buttons to AnywhereTrainer" />

		<VersionSettings gameVersion="1.4.8-RoR" windowsVersion="1.0" savedVariablesVersion="1.0" />

		<WARInfo>
			<Categories>
				<Category name="RVR" />
				<Category name="CAREER_SPECIFIC" />
				<Category name="OTHER" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name="MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>

		<Dependencies>
			<Dependency name="AnywhereTrainer" />
		</Dependencies>
		
		<Files>
			<File name="AnywhereTrainerAdditions.lua" />
			<File name="AnywhereTrainerAdditions.xml" />
		</Files>

        <OnInitialize>
            <CallFunction name="AnywhereTrainerAdditions.Initialize" />
        </OnInitialize>
        <OnShutdown>
            <CallFunction name="AnywhereTrainerAdditions.Shutdown" />
        </OnShutdown>
    </UiMod>
    
</ModuleFile>    