------------------------------------------------------------------
----  Global Variables
------------------------------------------------------------------

AnywhereTrainerAdditions = {}


------------------------------------------------------------------
----  local Variables
------------------------------------------------------------------

local bShinies = nil

------------------------------------------------------------------
----  Core Functions
------------------------------------------------------------------

function AnywhereTrainerAdditions.Initialize( )
	if not AnywhereTrainer then
		TextLogAddEntry("Chat", 0, L"AnywhereTrainerAdditions couldn't find AnywhereTrainer")
		return
	end
	
	-- Get supported mod data
	AnywhereTrainerAdditions.GetSupportedModData()


	-- add new buttons
	AnywhereTrainerAdditions.TabData = {
		{ 	Name="AnywhereTrainerTabAuction", Template="AnywhereTrainerTabTemplate", 
			TextureWindow="Icon", Slice="Tab-IronBreaker3-Brotherhood", --Tab-IronBreaker3-Brotherhood Tab-PassiveAbilities
			Anchor={ Point="bottomleft", RelativeTo="AnywhereTrainerTabTome", RelativePoint="topleft", X=0, Y=2 }, 
			Tooltip=L"Auction", OnLeftClick=AnywhereTrainerAdditions.OnLeftClickAuction, OnRightClick=AnywhereTrainerAdditions.OnRightClickAuction 
		},
		
		{ 	Name="AnywhereTrainerTabBank", 	Template="AnywhereTrainerTabTemplate", 
			TextureWindow="Icon", Slice="Tab-IronBreaker2-Stone", --Tab-IronBreaker2-Stone Tab-BlackOrc2-DaBoss
			Anchor={ Point="bottomleft", RelativeTo="AnywhereTrainerTabAuction", RelativePoint="topleft", X=0, Y=2 }, 
			Tooltip=L"Bank", OnLeftClick=AnywhereTrainerAdditions.OnLeftClickBank 
		},
		
		{ 	Name="AnywhereTrainerTabMail", 	Template="AnywhereTrainerTabTemplate", 
			TextureWindow="Icon", Slice="Tab-Magus1-Havoc", --Tab-Magus1-Havoc
			Anchor={ Point="bottomleft", RelativeTo="AnywhereTrainerTabBank", RelativePoint="topleft", X=0, Y=2 }, 
			Tooltip=L"Mail", OnLeftClick=AnywhereTrainerAdditions.OnLeftClickMail 
		},

		-- { 	Name="AnywhereTrainerBottomBookend", Template="AnywhereTrainerBottomBookendTemplate", 
		-- 	TextureWindow="", Slice="TabBottom", 
		-- 	Anchor={ Point="bottomleft", RelativeTo="AnywhereTrainerTabMail", RelativePoint="topleft", X=0, Y=2}
		-- } 
	}

	if bShinies then
		-- make auction tab use template that can handle rclick if using shinies (to open standard AH window)
		AnywhereTrainerAdditions.TabData[1].Template = "AnywhereTrainerAdditionsTabTemplate"
	end


	-- move anywheretrainer higher
	AnywhereTrainer.TabData[1].Anchor.Y = 100
	local tab = AnywhereTrainer.TabData[1]
	WindowClearAnchors("AnywhereTrainerTopBookend")
	WindowAddAnchor( tab.Name, tab.Anchor.Point, tab.Anchor.RelativeTo, tab.Anchor.RelativePoint, tab.Anchor.X, tab.Anchor.Y )

	-- create new windows
	for _, tab in ipairs(AnywhereTrainerAdditions.TabData) do
		CreateWindowFromTemplate( tab.Name, tab.Template, "CharacterWindow" )
		WindowAddAnchor( tab.Name, tab.Anchor.Point, tab.Anchor.RelativeTo, tab.Anchor.RelativePoint, tab.Anchor.X, tab.Anchor.Y )
		DynamicImageSetTextureSlice( tab.Name .. tab.TextureWindow, tab.Slice )
	end

	-- reanchor the BottomBookend window that was created by AnywhereTrainer originally, we place our windows there
	--DestroyWindow("AnywhereTrainerBottomBookend")
	local tab = AnywhereTrainer.TabData[#AnywhereTrainer.TabData]
	tab.Anchor.RelativeTo = "AnywhereTrainerTabMail"
	WindowClearAnchors("AnywhereTrainerBottomBookend")
	WindowAddAnchor( tab.Name, tab.Anchor.Point, tab.Anchor.RelativeTo, tab.Anchor.RelativePoint, tab.Anchor.X, tab.Anchor.Y )

	-- add new tabs to anywheretrainer for it to handle lclick
	local function TableConcat(t1,t2)
	    for i=1,#t2 do
	        t1[#t1+1] = t2[i]
	    end
	    return t1
	end

	AnywhereTrainer.TabData = TableConcat(AnywhereTrainer.TabData, AnywhereTrainerAdditions.TabData)
	
	-- fix being unable to drag things in bankwindow (sorry if i break something)
	AnywhereTrainerAdditions.BankWindowDraggingFixed = false
	AnywhereTrainerAdditions.FixBankWindowDragging(true)

	-- if you see this we're good (probably)
	TextLogAddEntry("Chat", 0, L"AnywhereTrainerAdditions initialized")
end



function AnywhereTrainerAdditions.GetSupportedModData()
	local supportedModData = {}
	supportedModData = ModulesGetData()

    for modIndex, modData in ipairs( supportedModData ) do
        if (modData.isEnabled == true) then
			bShinies = true
		else
			bShinies = false
		end
    end
end


function AnywhereTrainerAdditions.OnRButtonUp( )
	for _, tab in ipairs(AnywhereTrainer.TabData) do
		if( tab.Name .. "InactiveImage" == SystemData.MouseOverWindow.name ) then
			if( tab.OnRightClick ~= nil ) then
				tab.OnRightClick( )
			end
			return
		end
	end
end


------------------------------------------------------------------
---- Tabs Functions
------------------------------------------------------------------


function AnywhereTrainerAdditions.OnLeftClickBank( )
	if( not WindowGetShowing( "BankWindow" ) ) then
		--AnywhereTrainer.hookBankWindow(true)
		BankWindow.OpenBank()
	else
		--AnywhereTrainer.hookBankWindow(false)
		BankWindow.Hide( )
	end
end


function AnywhereTrainerAdditions.OnLeftClickAuction( )
	if bShinies then
		if( not WindowGetShowing( "ShiniesWindow" ) ) then
			Shinies.Show( )
		else
			Shinies.Hide( )
		end
	else

		if( not WindowGetShowing( "AuctionWindow" ) ) then
			AuctionWindow.Show( )
		else
			AuctionWindow.Hide( )
		end
	end
end

function AnywhereTrainerAdditions.OnRightClickAuction( )
	if( not WindowGetShowing( "AuctionWindow" ) ) then
		AuctionWindow.Show( )
	else
		AuctionWindow.Hide( )
	end
end

function AnywhereTrainerAdditions.OnLeftClickCraft( )

	if( GameData.TradeSkillLevels[GameData.TradeSkills.APOTHECARY] ~= nil and GameData.TradeSkillLevels[GameData.TradeSkills.APOTHECARY] > 0 ) then
		CraftingSystem.ToggleShowing(GameData.TradeSkills.APOTHECARY)
	elseif( GameData.TradeSkillLevels[GameData.TradeSkills.TALISMAN] ~= nil and GameData.TradeSkillLevels[GameData.TradeSkills.TALISMAN] > 0 ) then
		CraftingSystem.ToggleShowing( GameData.TradeSkills.TALISMAN )
	end
	--GameData.TradeSkills.CULTIVATION 
end

function AnywhereTrainerAdditions.OnLeftClickMail( )
	
	--BroadcastEvent(SystemData.Events.MAILBOX_RESULTS_UPDATED)
	if( not WindowGetShowing( "MailWindow" ) ) then
		-- BroadcastEvent(SystemData.Events.INTERACT_MAILBOX_OPEN)
		-- BroadcastEvent(SystemData.Events.MAILBOX_HEADER_UPDATED)
		-- BroadcastEvent(SystemData.Events.MAILBOX_HEADERS_UPDATED)
		MailWindow.OnOpen()
	else
		MailWindow.OnClose( )
	end
end

function AnywhereTrainerAdditions.OnRightClickMastery( )
	if WarBuilder then
		if( not WindowGetShowing( "warbuilderwindow" ) ) then
			WindowSetShowing("warbuilderwindow",true)
			--warbuilderCommand("")
		else
			--MailWindow.OnClose( )
			WindowSetShowing("warbuilderwindow",false)
		end
	end
end

------------------------------------------------------------------
----  Bank fix
------------------------------------------------------------------

function AnywhereTrainerAdditions.FixBankWindowDragging(enable)
	if enable and not AnywhereTrainerAdditions.BankWindowDraggingFixed then
		AnywhereTrainerAdditions.EquipmentLButtonDownOriginal = BankWindow.EquipmentLButtonDown
		BankWindow.EquipmentLButtonDown = AnywhereTrainerAdditions.EquipmentLButtonDown
		AnywhereTrainerAdditions.BankWindowDraggingFixed = true
	
	elseif not enable and AnywhereTrainerAdditions.BankWindowDraggingFixed then
		BankWindow.EquipmentLButtonDown = AnywhereTrainerAdditions.EquipmentLButtonDownOriginal
		AnywhereTrainerAdditions.BankWindowDraggingFixed = false
	end
end

function AnywhereTrainerAdditions.EquipmentLButtonDown( buttonIndex, flags )

	local slot = BankWindow.GetSlotNumberForButtonIndex( buttonIndex )  
    local itemData = BankWindow.GetItem(slot)

    if Cursor.IconOnCursor() then
        
        -- don't bother sending a move item if we're dropping on the original slot. just clear the cursor
        --   Same for list mode
        if( Cursor.Data.Source == Cursor.SOURCE_BANK and Cursor.Data.SourceSlot == slot ) then 
			
            Cursor.Clear()        
           
        else  
            RequestMoveItem( Cursor.Data.Source, Cursor.Data.SourceSlot, Cursor.SOURCE_BANK, slot, Cursor.Data.StackAmount )            
            BankWindow.dropPending = true
        end
        
    else
        if itemData.uniqueID > 0 then--DataUtils.IsValidItem( itemData ) then
            if flags == SystemData.ButtonFlags.SHIFT  then
                -- This doesn't send the item data for some reason
                EA_ChatWindow.InsertItemLink( itemData )
            elseif flags == SystemData.ButtonFlags.CONTROL  then
            	-- get first empty inv slot
            	local invitems = DataUtils.GetItems()
            	if invitems then 
            		local firstslot = 0 
            		--GameData.Inventory.FIRST_AVAILABLE_INVENTORY_SLOT is always 561 whatever that means. 
            		--if using this value, calling RequestMoveItem will equip things on character
	            	for i, item in ipairs(invitems) do
						if item.id == 0 then
							firstslot = i
							break
						end
					end 
					if firstslot > 0 then
	                	RequestMoveItem( Cursor.SOURCE_BANK, slot, Cursor.SOURCE_INVENTORY, firstslot, itemData.stackCount)
	            	end
	            end
            else
                Cursor.PickUp( Cursor.SOURCE_BANK, slot, itemData.uniqueID, itemData.iconNum, true)
            end
        end
        BankWindow.dropPending = false
    end
        
end 